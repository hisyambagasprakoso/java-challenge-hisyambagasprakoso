package ist.challenge.hisyam.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import ist.challenge.hisyam.Model.Registrasi;

@Repository
public interface RegistrasiRepository extends JpaRepository<Registrasi,String>{
    Registrasi findByUsername(String username);
}
