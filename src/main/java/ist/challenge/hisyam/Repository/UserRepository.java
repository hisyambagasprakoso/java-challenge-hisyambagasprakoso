package ist.challenge.hisyam.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import ist.challenge.hisyam.Model.Registrasi;

@Repository
public interface UserRepository extends PagingAndSortingRepository<Registrasi,Long> {
}
