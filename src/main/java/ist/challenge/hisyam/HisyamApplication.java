package ist.challenge.hisyam;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HisyamApplication {

	public static void main(String[] args) {
		SpringApplication.run(HisyamApplication.class, args);
	}

}
