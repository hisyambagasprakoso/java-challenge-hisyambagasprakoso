package ist.challenge.hisyam.Controller;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;


import ist.challenge.hisyam.Model.Registrasi;
import ist.challenge.hisyam.Repository.RegistrasiRepository;
import ist.challenge.hisyam.Repository.UserRepository;


@RestController
public class RegistrasiController {
    // @Override
    @Autowired
    RegistrasiRepository registrasi_repo;
    @Autowired
    UserRepository user_repo;
    // HttpStatus.Series http;
    String result = "";

    @GetMapping("/api/list_user")
    @ResponseBody
    public List<Registrasi> getListUser(){
        return registrasi_repo.findAll();
    }

    @PutMapping("/api/list_user/{id}")
    @ResponseStatus(HttpStatus.OK)
    public String update(@PathVariable("id") Long id, @RequestBody Registrasi registrasi) throws IOException{
        URL url = new URL("http://localhost:8080/api/list_user/{id}");
        HttpURLConnection connection = (HttpURLConnection)url.openConnection();
        connection.setRequestMethod("GET");
        connection.connect();
        int statusCode = connection.getResponseCode();
        Registrasi rgs = user_repo.findById(id).get();
        String usrm = rgs.getUsername();
        String usrm2 = registrasi.getUsername();
        String pw = rgs.getPassword();
        String pw2 = registrasi.getPassword();
        System.out.print(statusCode);
        if(usrm.equals(usrm2)){
            result = "Username sudah ada";
        }else{
            if(pw.equals(pw2)){
                result = "Password tidak boleh sama dengan password sebelumnya";
            }else{
                result="HTTP Status Code 201";
                BeanUtils.copyProperties(registrasi, rgs);
                registrasi_repo.save(rgs);
            }
        }
        return result;
    }

    @PostMapping("/api/registrasi")
    @ResponseStatus(HttpStatus.CREATED)
    public String insert(@RequestBody @Valid Registrasi registrasi) throws IOException{
        URL url = new URL("http://localhost:8080/api/registrasi");
        HttpURLConnection connection = (HttpURLConnection)url.openConnection();
        connection.setRequestMethod("GET");
        connection.connect();
        int statusCode = connection.getResponseCode();
        System.out.print(result);
        if(statusCode==405){
            result= "HTTP Status Code 201";
            // System.out.print(statusCode);    
            registrasi_repo.save(registrasi);  
        }   
        return result;      
    }

    @PostMapping("api/login")
    @ResponseStatus(HttpStatus.CREATED)
    public String Login(@RequestBody @Valid Registrasi registrasi){
         String usrm = registrasi.getUsername();
        String usrm2 = registrasi_repo.findByUsername(usrm).getUsername();
        String pw = registrasi.getPassword();
        String pw2 = registrasi_repo.findByUsername(usrm).getPassword();
        if(usrm.equals(usrm2)&&pw.equals(pw2) ){
            result = "HTTP Status Code 200";
        }
        if(usrm.equals(usrm2) && !pw.equals(pw2)){
            result = "HTTP Status Code 401";
        }
        return result;
    }

    @RequestMapping(value = "/users/find", method = RequestMethod.GET)
    @ResponseBody
    public Registrasi findByUsername(@RequestParam("username") String username) {
        return registrasi_repo.findByUsername(username);
    }
}
